package notes.tutorial.notes;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SharedPrefsUtil {

    private final static String KEY = "notes";
    private final static String APP_PREFS = "notes_pref";

    public static void storeNewNote(Context context, Note note){
        Gson gson = new Gson();
        SharedPreferences preferences= context.getSharedPreferences(APP_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String notesString = preferences.getString(KEY, null);
        List<Note> notes = new ArrayList<>();
        if(notesString!=null){
            Type listType = new TypeToken<List<Note>>(){}.getType();
            notes = gson.fromJson(notesString, listType);
        }
        notes.add(note);
        String newNotesString = gson.toJson(notes);
        editor.putString(KEY,newNotesString);
        editor.commit();
    }

    public static List<Note> getListNote(Context context){
        Gson gson = new Gson();
        SharedPreferences preferences= context.getSharedPreferences(APP_PREFS,Context.MODE_PRIVATE);
        String notesString = preferences.getString(KEY,null);
        if(notesString==null){
            return new ArrayList<>();
        }
        Type listType = new TypeToken<List<Note>>(){}.getType();
        return gson.fromJson(notesString, listType);
    }

    public static void storeNewNotes(Context context, List<Note> notes){
        Gson gson = new Gson();
        SharedPreferences preferences= context.getSharedPreferences(APP_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String newNotesString = gson.toJson(notes);
        editor.putString(KEY,newNotesString);
        editor.commit();
    }

    public static List<Note> deleteNote(Context context, Note note){
        Gson gson = new Gson();
        SharedPreferences preferences= context.getSharedPreferences(APP_PREFS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String notesString = preferences.getString(KEY, null);
        List<Note> notes = new ArrayList<>();
        if(notesString!=null){
            Type listType = new TypeToken<List<Note>>(){}.getType();
            notes = gson.fromJson(notesString, listType);
        }
        notes.remove(note);
        String newNotesString = gson.toJson(notes);
        editor.putString(KEY,newNotesString);
        editor.commit();
        return notes;
    }
}
