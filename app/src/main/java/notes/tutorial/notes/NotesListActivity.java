package notes.tutorial.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class NotesListActivity extends AppCompatActivity {

    private final int REQUEST_CODE_ADD = 1234;
    private final int REQUEST_CODE_EDIT = 12345;
    private final String NOTES_KEY = "my_notes";
    private ListView notesListView;
    private List<Note> notes = new ArrayList<>();
    private NoteAdapter notesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),NewNoteActivity.class);
                startActivityForResult(intent,REQUEST_CODE_ADD);
            }
        });

        if(savedInstanceState!=null){
            notes = (List<Note>) savedInstanceState.getSerializable(NOTES_KEY);
        }
        else{
            notes = SharedPrefsUtil.getListNote(getApplicationContext());
        }

        notesListView = findViewById(R.id.notes_list);
        notesListAdapter = new NoteAdapter(getApplicationContext(),R.id.notes_list,R.layout.note_row,notes, REQUEST_CODE_EDIT);
        notesListView.setAdapter(notesListAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("resume","resume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("pause","pause called");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d("save state","save state called");
        outState.putSerializable(NOTES_KEY,(Serializable)notes);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notes_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"Settings pressed",Toast.LENGTH_LONG).show();
            return true;
        }
        if(id == R.id.action_demo){
            Toast.makeText(getApplicationContext(),"Demo pressed",Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==REQUEST_CODE_ADD){
            if(resultCode== Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                String title = extras.getString("title");
                String content = extras.getString("content");
                Log.i("received title",title);
                Log.i("received content",content);
                Note note = new Note(title, content, UUID.randomUUID().toString());
                notes.add(note);
                notesListAdapter.notifyDataSetChanged();
                SharedPrefsUtil.storeNewNote(getApplicationContext(), note);
            }
        }
        if(requestCode==REQUEST_CODE_EDIT){
            if(resultCode== Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                String title = extras.getString("title");
                String content = extras.getString("content");
                String uuid = extras.getString("uuid");
                for(Note note : notes){
                    if(note.getUuid().equals(uuid)){
                        note.setContent(content);
                        note.setTitle(title);
                        break;
                    }
                }
                notesListAdapter.notifyDataSetChanged();
                SharedPrefsUtil.storeNewNotes(getApplicationContext(),notes);
            }
        }
    }
}
