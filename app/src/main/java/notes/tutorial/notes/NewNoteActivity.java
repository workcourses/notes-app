package notes.tutorial.notes;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Serializable;

public class NewNoteActivity extends AppCompatActivity {

    private EditText titleEditText;
    private EditText contentEditText;
    private Button addButton;
    private String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        uuid = null;

        titleEditText = findViewById(R.id.title_input);
        contentEditText = findViewById(R.id.content_input);
        addButton = findViewById(R.id.save_button);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras!=null) {
            Note note = (Note) extras.getSerializable("note");
            if (note != null) {
                titleEditText.setText(note.getTitle());
                contentEditText.setText(note.getContent());
                addButton.setText("Editeaza");
                uuid = note.getUuid();
            }
        }

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("title",titleEditText.getText().toString());
                Log.i("content",contentEditText.getText().toString());
                Toast.makeText(getApplicationContext(),"Note added", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(),NotesListActivity.class);
                intent.putExtra("title",titleEditText.getText().toString());
                intent.putExtra("content",contentEditText.getText().toString());
                intent.putExtra("uuid",uuid);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notes_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(getApplicationContext(),"New settings pressed",Toast.LENGTH_LONG).show();
            return true;
        }
        if(id == R.id.action_demo){
            Toast.makeText(getApplicationContext(),"New demo pressed",Toast.LENGTH_LONG).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
