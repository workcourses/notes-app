package notes.tutorial.notes;

import java.io.Serializable;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Note implements Serializable {
    private String title;
    private String content;
    private String uuid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return uuid.equals(note.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
