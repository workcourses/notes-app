package notes.tutorial.notes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    private List<Note> notes;
    private Integer REQUEST_CODE_EDIT;

    public NoteAdapter(Context context, int resource, int textViewResourceId, List<Note> objects, Integer REQUEST_CODE_EDIT) {
        super(context, resource, textViewResourceId, objects);
        this.notes = objects;
        this.REQUEST_CODE_EDIT = REQUEST_CODE_EDIT;
    }

    @Override
    public int getCount() {
        return notes.size();
    }

    @Override
    public Note getItem(int position) {
        return notes.get(position);
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        final Note note = getItem(position);

        final Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View rowView = layoutInflater.inflate(R.layout.note_row, parent, false);
        TextView titleLabel = rowView.findViewById(R.id.title_label);
        titleLabel.setText(note.getTitle());

        titleLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,NewNoteActivity.class);
                intent.putExtra("note",note);
                ((Activity)context).startActivityForResult(intent,REQUEST_CODE_EDIT);
            }
        });

        TextView contentLabel = rowView.findViewById(R.id.content_label);
        contentLabel.setText(note.getContent());

        ImageButton deleteButton = rowView.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"delete pressed",Toast.LENGTH_LONG).show();
                notes.clear();
                notes.addAll(SharedPrefsUtil.deleteNote(context,note));
                notifyDataSetChanged();
            }
        });

        return rowView;
    }
}
